% run_real_vs_complex_shifts_fixed

% parameters
k = 32;                            % fixed order
eta = 1;                           % [eV]
omega = linspace(540,600,1000).';  % [eV]
opts = struct('tol',1e-6);         % gmres tolerance
filename = 'water_cluster_5H2O';
problem = 'cluster of 5 H_2O molecules';

% load
load([filename,'.mat'],'A','B','dx','dy','dz','eval','oscstr');

% full Hamiltonian + complex shifts
fprintf('\nFULL HAMILTONIAN + COMPLEX SHIFTS\n');
t = tic;
[sigma_2nc,~,info_2nc] = mor_spectrum(A,B,dx,dy,dz,omega,eta,k,0,0,1,opts);
t2nc = toc(t); it2nc = info_2nc.it;

% full Hamiltonian + real shifts
fprintf('\nFULL HAMILTONIAN + REAL SHIFTS\n');
t = tic;
[sigma_2nr,~,info_2nr] = mor_spectrum(A,B,dx,dy,dz,omega,eta,k,0,1,1,opts);
t2nr = toc(t); it2nr = info_2nr.it;

% M and K + complex shifts
fprintf('\nM and K + COMPLEX SHIFTS\n');
t = tic;
[sigma_nc,~,info_nc] = mor_spectrum(A,B,dx,dy,dz,omega,eta,k,1,0,1,opts);
tnc = toc(t); itnc = info_nc.it;

% M and K + real shifts
fprintf('\nM and K + REAL SHIFTS\n');
t = tic;
[sigma_nr,~,info_nr] = mor_spectrum(A,B,dx,dy,dz,omega,eta,k,1,1,1,opts);
tnr = toc(t); itnr = info_nr.it;

% timings
fprintf('\nTIMINGS (fixed order k = %i):\n',k);
fprintf('  * full H + complex shifts: %9.3f sec\n',t2nc);
fprintf('  * full H + real shifts:    %9.3f sec\n',t2nr);
fprintf('  * M & K  + complex shifts: %9.3f sec\n',tnc);
fprintf('  * M & K  + real shifts:    %9.3f sec\n',tnr);

% spectrum via eigenvalues and oscillator strenghts
spectrum = eig_spectrum(eval,oscstr,omega,eta);

% plot
figure;
plot(omega,[spectrum,sigma_2nc,sigma_2nr,sigma_nc,sigma_nr]);
legend('Eigensystem','2n - complex','2n - real','n - complex','n - real');
xlabel('Energy [eV]');
ylabel('Absorption Spectrum (Arb.)');
title([problem,'   -   k = ',num2str(k)]);

% save
clear A B dx dy dz eval oscstr
save results_real_vs_complex_shifts_fixed.mat
