% run_5H2O

% parameters
tol = 1e-2;                        % tolerance for addaptive order
eta = 1;                           % [eV]
omega = linspace(540,600,1000).';  % [eV]
opts = struct('tol',1e-6);         % gmres tolerance
filename = 'water_cluster_5H2O';
problem = 'cluster of 5 H_2O molecules';

% load
load([filename,'.mat'],'A','B','dx','dy','dz','eval','oscstr');

% M and K + complex shifts
t = tic;
[sigma,sys] = mor_spectrum(A,B,dx,dy,dz,omega,eta,tol,1,0,1,opts);
telapsed = toc(t);

% order of reduced model
k = size(sys.x.A,1);

% spectrum via eigenvalues and oscillator strenghts
spectrum = eig_spectrum(eval,oscstr,omega,eta);

% plot
figure;
plot(omega,[spectrum,sigma]);
legend('Eigensystem',['MOR (k = ',num2str(k),')']);
xlabel('Energy [eV]');
ylabel('Absorption Spectrum (Arb.)');
params = sprintf('\\eta = %d eV, tol = %.1g, GMRES tol = %.0e',eta,tol,opts.tol);
title([problem,'   -   ',params]);

% save
save(['results_',filename,'.mat'],'tol','eta','omega','sigma',...
    'sys','k','telapsed');
