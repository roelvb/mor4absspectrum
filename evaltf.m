function y = evaltf(A,E,b,c,omega)
%EVALTF   Evaluate transfer function
%   y = EVALTF(A,E,b,c,shifts) returns the transfer function
%
%       y(omega) = c' * inv( omega*E - A ) * b
%
%   of the linear dynamical system
%
%       ( omega*E - A ) x = b
%                       y = c' * x
%
%   evaluated at omega.
%
%   See also MOR_SAMPLING.
%
%   Author:  Roel Van Beeumen
%   Version: April 11, 2017

k = length(omega);
y = zeros(k,1);
parfor i = 1:k
    y(i) = c'*((omega(i)*E - A)\b);
end

end
