function [sigma,scaling] = eig_spectrum(eval,oscstr,omega,eta,type)
% MATLAB function to generate broadened spectra from the eigenmodes of TD-DFT
%
% Copyright (C) 2017. All Rights Reserved.
% David Williams-Young
% Department of Chemistry
% University of Washington
% dbwy@u.washington.edu

%% check inputs
if nargin < 5, type = 'lorentzian'; end

%% spectrum
sigma = zeros(size(omega));
start = false;
for i = 1:size(eval,1)
    
    if start && eval(i) >= omega(end)
        break;
    end
    
    if ~start && eval(i) >= omega(1)
        start = true;
    end
    
    if start
        if strcmpi(type,'gaussian')
            sigma = sigma + oscstr(i) / sqrt(2 * eta * pi) * ...
                exp(-(omega - eval(i)).^2 / (2 * eta));
        else % 'lorentzian'
            sigma = sigma + oscstr(i) * ...
                (eta*pi * ( 1 + ((omega - eval(i)) / eta).^2)).^(-1);
        end
    end
    
end
scaling = max(sigma);
sigma = omega(:).*sigma(:);
sigma = sigma/max(sigma);

end
