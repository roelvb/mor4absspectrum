% run_real_vs_complex_shifts_adaptive

% parameters
tol = 1e-2;                        % tolerance for adaptive order
eta = 1;                           % [eV]
omega = linspace(540,600,1000).';  % [eV]
opts = struct('tol',1e-6);         % gmres tolerance
filename = 'water_cluster_5H2O';
problem = 'cluster of 5 H_2O molecules';

% load
load([filename,'.mat'],'A','B','dx','dy','dz','eval','oscstr');

% full Hamiltonian + complex shifts
fprintf('\nFULL HAMILTONIAN + COMPLEX SHIFTS\n');
t = tic;
[sigma_2nc,sys_2nc,info_2nc] = mor_spectrum(A,B,dx,dy,dz,omega,eta,tol,0,0,1,opts);
t2nc = toc(t); k_2nc = size(sys_2nc.x.A,1); it2nc = info_2nc.it;

% full Hamiltonian + real shifts
fprintf('\nFULL HAMILTONIAN + REAL SHIFTS\n');
t = tic;
[sigma_2nr,sys_2nr,info_2nr] = mor_spectrum(A,B,dx,dy,dz,omega,eta,tol,0,1,1,opts);
t2nr = toc(t); k_2nr = size(sys_2nr.x.A,1); it2nr = info_2nr.it;

% M and K + complex shifts
fprintf('\nM and K + COMPLEX SHIFTS\n');
t = tic;
[sigma_nc,sys_nc,info_nc] = mor_spectrum(A,B,dx,dy,dz,omega,eta,tol,1,0,1,opts);
tnc = toc(t); k_nc = size(sys_nc.x.A,1); itnc = info_nc.it;

% M and K + real shifts
fprintf('\nM and K + REAL SHIFTS\n');
t = tic;
[sigma_nr,sys_nr,info_nr] = mor_spectrum(A,B,dx,dy,dz,omega,eta,tol,1,1,1,opts);
tnr = toc(t); k_nr = size(sys_nr.x.A,1); itnr = info_nr.it;

% timings
fprintf('\nTIMINGS (adaptive order k):\n');
fprintf('  * full H + complex shifts  (k = %3i): %9.3f sec\n',k_2nc,t2nc);
fprintf('  * full H + real shifts     (k = %3i): %9.3f sec\n',k_2nr,t2nr);
fprintf('  * M & K  + complex shifts  (k = %3i): %9.3f sec\n',k_nc,tnc);
fprintf('  * M & K  + real shifts     (k = %3i): %9.3f sec\n',k_nr,tnr);

% spectrum via eigenvalues and oscillator strenghts
spectrum = eig_spectrum(eval,oscstr,omega,eta);

% plot
figure;
plot(omega,[spectrum,sigma_2nc,sigma_2nr,sigma_nc,sigma_nr]);
lgnd = cell(5,1);
lgnd{1} = 'Eigensystem';
lgnd{2} = sprintf('2n - complex (k = %i)',k_2nc);
lgnd{3} = sprintf(   '2n - real (k = %i)',k_2nr);
lgnd{4} = sprintf( 'n - complex (k = %i)',k_nc);
lgnd{5} = sprintf(    'n - real (k = %i)',k_nr);
legend(lgnd);
xlabel('Energy [eV]');
ylabel('Absorption Spectrum (Arb.)');
title([problem,'   -   adaptive order k']);

% save
clear A B dx dy dz eval oscstr
save results_real_vs_complex_shifts_adaptive.mat
