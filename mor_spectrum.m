function [sigma,sys,info] = mor_spectrum(A,B,dx,dy,dz,omega,eta,k,...
    real_AB,real_shifts,use_gmres,opts,make_plot)
%MOR_SPECTRUM   Absorption spectrum via model order reduction
%   sigma = MOR_SPECTRUM(A,B,dx,dy,dz,omega,eta,k) returns the normalized
%   absorption spectrum at the frequencies omega [eV]. A is Hermitian, B is
%   complex symmetric, and dx, dy, and dz are dipole vectors of the same size
%   as A and B. The broadening factor is eta [eV] and k is the order of the
%   reduced order models.
%
%   sigma = MOR_SPECTRUM(A,B,dx,dy,dz,omega,eta,tol) returns the normalized
%   absorption spectrum but uses the tolerance tol for the adaptive order
%   determination of the reduced order models.
%
%   MOR_SPECTRUM(...,real_AB) uses structure exploiting model order reduction
%   if real_AB is true (A and B should be real).
%
%   MOR_SPECTRUM(...,real_AB,real_shifts) uses real interpolation points if
%   real_shifts is true. Otherwise complex ones are used.
%
%   MOR_SPECTRUM(...,real_AB,real_shifts,use_gmres,opts) uses gmres to solve
%   the linear systems if use_gmres is true. Otherwise \ is used. The gmres
%   parameters for MOR_SAMPLING or MOR_SAMPLING_MK are specified in opts.
%
%   MOR_SPECTRUM(...,real_AB,real_shifts,use_gmres,opts,make_plot) generates
%   plots for the adaptive order determination if make_plot is true.
%
%   [sigma,sys] = MOR_SPECTRUM(A,B,dx,dy,dz,k,omega,eta) also returns the
%   reduced order models:
%     sys.x = reduced order model for dipole vector x
%     sys.y = reduced order model for dipole vector y
%     sys.z = reduced order model for dipole vector z
%
%   [sigma,sys,info] = MOR_SPECTRUM(A,B,dx,dy,dz,k,omega,eta) also returns the
%   number of gmres iterations and the adaptively selected shifts:
%     info.shifts  selected shifts
%     info.it      total number of gmres iterations
%
%   See also MOR_SAMPLING, MOR_SAMPLING_MK, EVALTF.
%
%   Author:  Roel Van Beeumen
%   Version: September 1, 2017

%% check inputs
if nargin < 9, real_AB = false; end
if nargin < 10, real_shifts = false; end
if nargin < 11, use_gmres = true; end
if nargin < 12, opts = []; end
if nargin < 13, make_plot = false; end

%% dimension
n = size(A,1);

%% shifts
shifts = generate_shifts(omega,eta,k,real_shifts);

%% reduced order models
if n > 500, fprintf('* Constructing matrices...\n'); end
if ~real_AB
    
    H = [A, B; conj(B), conj(A)];
    S = spdiags([ones(n,1);-ones(n,1)],0,2*n,2*n);
    Dx = [dx;conj(dx)];
    Dy = [dy;conj(dy)];
    Dz = [dz;conj(dz)];

    [sigma,sys,info] = reduced_spectrum(H,S,Dx,Dy,Dz,omega,eta,k,...
        use_gmres,opts,shifts,make_plot);
    
else
    
    K = A - B;
    M = A + B;
    
    [sigma,sys,info] = reduced_spectrum_MK(M,K,dx,dy,dz,omega,eta,k,...
        use_gmres,opts,shifts,make_plot);
    
end

end


function shifts = generate_shifts(omega,eta,k,real_shifts)

%% interval
omin = omega(1);
omax = omega(end);

if floor(k) == k
    %% k is dimension of reduced order model
    shifts = linspace(omin,omax,k).';
    if ~real_shifts
        shifts = shifts + 1i*eta;
    end
    
else
    %% k is tolerance for reduced order model
    % parameters
    npts = 17;
    levels = 8;
    
    % shifts
    shifts = cell(levels,1);
    shifts{1} = linspace(omin,omax,npts).';
    for i = 2:levels
        npts = 2*npts - 1;
        t = linspace(omin,omax,npts).';
        shifts{i} = t(2:2:end);
    end
    
    % complex shifts
    if ~real_shifts
        for i = 1:levels
            shifts{i} = shifts{i} + 1i*eta;
        end
    end
    
end

end


function [sigma,sys,info] = reduced_spectrum(H,S,Dx,Dy,Dz,omega,eta,k,...
    use_gmres,opts,shifts,make_plot)

%% constant
Eh2eV = 27.21138602;

%% points
points = (omega + 1i*eta)/Eh2eV;

if floor(k) == k
    %% reduced order model of dimension k
    
    % reduced systems
    fprintf('* Dipole x:\n');
    [Ax,Ex,bx,cx,Vx,infox] = mor_sampling(H,S,Dx,Dx,shifts/Eh2eV,[],use_gmres,opts);
    sysx = struct('A',Ax,'E',Ex,'b',bx,'c',cx,'V',Vx);
    fprintf('* Dipole y:\n');
    [Ay,Ey,by,cy,Vy,infoy] = mor_sampling(H,S,Dy,Dy,shifts/Eh2eV,[],use_gmres,opts);
    sysy = struct('A',Ay,'E',Ey,'b',by,'c',cy,'V',Vy);
    fprintf('* Dipole z:\n');
    [Az,Ez,bz,cz,Vz,infoz] = mor_sampling(H,S,Dz,Dz,shifts/Eh2eV,[],use_gmres,opts);
    sysz = struct('A',Az,'E',Ez,'b',bz,'c',cz,'V',Vz);
    sys = struct('x',sysx,'y',sysy,'z',sysz);
    
    % reduced spectrum
    sigmax = abs(imag(evaltf(Ax,Ex,bx,cx,points)));
    sigmay = abs(imag(evaltf(Ay,Ey,by,cy,points)));
    sigmaz = abs(imag(evaltf(Az,Ez,bz,cz,points)));
    sigma = omega.*(sigmax + sigmay + sigmaz);
    sigma = sigma/max(sigma);
    
    % info
    if use_gmres
        info = struct('shifts',shifts,'it',infox.it + infoy.it + infoz.it);
    else
        info = struct('shifts',shifts);
    end
    
else
    %% reduced order model for tolerance k
    
    % initialize plot
    if make_plot, figure; subplot(3,1,[1 2]); subplot(3,1,3); pause(1); end
    
    % adaptive shift selection loop
    Vx = []; Vy = []; Vz = []; Sigma = []; O = 0; e = inf(size(points));
    it = 0; sel = cell(1);
    for i = 1:length(shifts)
        
        % select shifts in unconverged intervals
        selection = select_shifts(shifts{i}/Eh2eV,points,e,k);
        if isempty(selection)
            break;
        else
            fprintf('* Level %i (%i shifts):\n',i,length(selection));
            sel{i,1} = selection;
        end
        
        % update reduced systems
        fprintf('--> dipole x:\n');
        [Ax,Ex,bx,cx,Vx,infox] = mor_sampling(H,S,Dx,Dx,selection,Vx,use_gmres,opts);
        fprintf('--> dipole y:\n');
        [Ay,Ey,by,cy,Vy,infoy] = mor_sampling(H,S,Dy,Dy,selection,Vy,use_gmres,opts);
        fprintf('--> dipole z:\n');
        [Az,Ez,bz,cz,Vz,infoz] = mor_sampling(H,S,Dz,Dz,selection,Vz,use_gmres,opts);
        
        % reduced systems
        sigmax = abs(imag(evaltf(Ax,Ex,bx,cx,points)));
        sigmay = abs(imag(evaltf(Ay,Ey,by,cy,points)));
        sigmaz = abs(imag(evaltf(Az,Ez,bz,cz,points)));
        sigma = omega.*(sigmax + sigmay + sigmaz);
        sigma = sigma/max(sigma);
        Sigma = [Sigma,sigma];
        O = [O;size(Vx,2)];
        
        % relative error
        if i > 1, e = abs(sigma - Sigma(:,i-1)); end
        
        % plot absorption spectrum
        if make_plot, update_plot(omega,sigma,e,O(i),O(i+1)); end
        
        % info
        if use_gmres, it = it + infox.it + infoy.it + infoz.it; end
        
    end
    
    % make system
    sysx = struct('A',Ax,'E',Ex,'b',bx,'c',cx,'V',Vx);
    sysy = struct('A',Ay,'E',Ey,'b',by,'c',cy,'V',Vy);
    sysz = struct('A',Az,'E',Ez,'b',bz,'c',cz,'V',Vz);
    sys = struct('x',sysx,'y',sysy,'z',sysz);
    
    % info
    if use_gmres
        info = struct('shifts',{sel},'it',it);
    else
        info = struct('shifts',{sel});
    end
    
end

end


function [sigma,sys,info] = reduced_spectrum_MK(M,K,dx,dy,dz,omega,eta,k,...
    use_gmres,opts,shifts,make_plot)

%% constant
Eh2eV = 27.21138602;

%% points
points = (omega + 1i*eta)/Eh2eV;

if floor(k) == k
    %% reduced order model of dimension k
    
    % reduced systems
    fprintf('* Dipole x:\n');
    [Ax,Ex,bx,cx,Vx,infox] = mor_sampling_MK(M,K,dx,(shifts/Eh2eV).^2,[],use_gmres,opts);
    sysx = struct('A',Ax,'E',Ex,'b',bx,'c',cx,'V',Vx);
    fprintf('* Dipole y:\n');
    [Ay,Ey,by,cy,Vy,infoy] = mor_sampling_MK(M,K,dy,(shifts/Eh2eV).^2,[],use_gmres,opts);
    sysy = struct('A',Ay,'E',Ey,'b',by,'c',cy,'V',Vy);
    fprintf('* Dipole z:\n');
    [Az,Ez,bz,cz,Vz,infoz] = mor_sampling_MK(M,K,dz,(shifts/Eh2eV).^2,[],use_gmres,opts);
    sysz = struct('A',Az,'E',Ez,'b',bz,'c',cz,'V',Vz);
    sys = struct('x',sysx,'y',sysy,'z',sysz);
    
    % reduced spectrum
    sigmax = abs(imag(evaltf(Ax,Ex,bx,cx,points.^2)));
    sigmay = abs(imag(evaltf(Ay,Ey,by,cy,points.^2)));
    sigmaz = abs(imag(evaltf(Az,Ez,bz,cz,points.^2)));
    sigma = omega.*(sigmax + sigmay + sigmaz);
    sigma = sigma/max(sigma);
    
    % info
    if use_gmres
        info = struct('shifts',shifts,'it',infox.it + infoy.it + infoz.it);
    else
        info = struct('shifts',shifts);
    end
    
else
    %% reduced order model for tolerance k
    
    % initialize plot
    if make_plot, figure; subplot(3,1,[1 2]); subplot(3,1,3); pause(1); end
    
    % adaptive shift selection loop
    Vx = []; Vy = []; Vz = []; Sigma = []; O = 0; e = inf(size(points));
    it = 0; sel = cell(1);
    for i = 1:length(shifts)
        
        % select shifts in unconverged intervals
        selection = select_shifts(shifts{i}/Eh2eV,points,e,k);
        if isempty(selection)
            break;
        else
            fprintf('* Level %i (%i shifts):\n',i,length(selection));
            sel{i,1} = selection;
        end
        
        % update reduced systems
        fprintf('--> dipole x:\n');
        [Ax,Ex,bx,cx,Vx,infox] = mor_sampling_MK(M,K,dx,selection.^2,Vx,use_gmres,opts);
        fprintf('--> dipole y:\n');
        [Ay,Ey,by,cy,Vy,infoy] = mor_sampling_MK(M,K,dy,selection.^2,Vy,use_gmres,opts);
        fprintf('--> dipole z:\n');
        [Az,Ez,bz,cz,Vz,infoz] = mor_sampling_MK(M,K,dz,selection.^2,Vz,use_gmres,opts);
        
        % reduced systems
        sigmax = abs(imag(evaltf(Ax,Ex,bx,cx,points.^2)));
        sigmay = abs(imag(evaltf(Ay,Ey,by,cy,points.^2)));
        sigmaz = abs(imag(evaltf(Az,Ez,bz,cz,points.^2)));
        sigma = omega.*(sigmax + sigmay + sigmaz);
        sigma = sigma/max(sigma);
        Sigma = [Sigma,sigma];
        O = [O;size(Vx,2)];
        
        % relative error
        if i > 1, e = abs(sigma - Sigma(:,i-1)); end
        
        % plot absorption spectrum
        if make_plot, update_plot(omega,sigma,e,O(i),O(i+1)); end
        
        % info
        if use_gmres, it = it + infox.it + infoy.it + infoz.it; end
        
    end
    
    % make system
    sysx = struct('A',Ax,'E',Ex,'b',bx,'c',cx,'V',Vx);
    sysy = struct('A',Ay,'E',Ey,'b',by,'c',cy,'V',Vy);
    sysz = struct('A',Az,'E',Ez,'b',bz,'c',cz,'V',Vz);
    sys = struct('x',sysx,'y',sysy,'z',sysz);
    
    % info
    if use_gmres
        info = struct('shifts',{sel},'it',it);
    else
        info = struct('shifts',{sel});
    end

end

end


function shifts = select_shifts(cand,omega,fun,tol)

K = length(cand);
N = length(omega);

check = fun <= tol;
shifts = [];

k = 1;
b = real(cand(k)) + (real(cand(k+1)) - real(cand(k)))/2;
idx = 1;

for i = 1:N
    if real(omega(i)) >= b
        % check previous interval
        if ~all(check(idx:i-1))
            shifts(end+1,1) = cand(k);
        end
        idx = i;
        % update interval
        k = k + 1;
        if k < K
            b = real(cand(k)) + (real(cand(k+1)) - real(cand(k)))/2;
        else
            b = real(omega(end));
        end
    end
end

end


function update_plot(omega,sigma,e,k0,k1)

%% absorption spectrum
subplot(3,1,[1 2]); plot(omega,sigma/max(sigma),'k');
xlabel('energy [eV]'); ylabel('absorption spectrum');
legend(['k = ',num2str(k1)]);

%% relative error
if k0 > 0
    subplot(3,1,3); semilogy(omega,e); hold on;
    xlabel('energy [eV]'); ylabel('relative error');
    ylim([1e-20,1e5]);
    l = sprintf('|\\sigma_{%i} - \\sigma_{%i}|',k1,k0);
    lgnd = legend;
    if isempty(lgnd)
        legend(l);
    else
        legend({lgnd.String{:},l});
    end
end
pause(1);

end
