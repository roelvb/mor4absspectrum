function [At,Et,bt,ct,V,info] = mor_sampling_MK(M,K,d,shifts,V0,use_gmres,opts)
%MOR_SAMPLING_MK   Moment matching model order reduction via sampling
%   [At,Et,bt,ct] = MOR_SAMPLING_MK(M,K,d,shifts) returns the reduced order
%   model which matches moments in the points given by shifts. The dimension
%   of the reduced order model is equal to the length of shifts.
%
%   MOR_SAMPLING_MK(M,K,d,shifts,V0) expands the constructed subspace with V0.
%   The dimension of the reduced order model is in this case equal to the sum
%   of the length of shifts and the number of columns of V0.
%
%   MOR_SAMPLING_MK(M,K,d,shifts,V0,use_gmres) uses gmres to solve the linear
%   systems if use_gmres is true. Otherwise \ is used.
%
%   MOR_SAMPLING_MK(M,K,d,shifts,V0,use_gmres,opts) specifies the gmres
%   parameters:
%     opts.restart:  gmres restart parameter
%                    [ [] ]
%     opts.tol:      gmres tolerance parameter
%                    [ [] ]
%     opts.maxit:    gmres maximum number of outer iterations parameter
%                    [ 100 ]
%     opts.P:        preconditioner for M*K
%                    [ spdiags(diag(M*K),0,n,n) ]
%     opts.x0:       gmres first initial guess parameter
%                    [ [] ]
%
%   [At,Et,bt,ct,V,info] = MOR_SAMPLING_MK(M,K,d,shifts,...) also returns the
%   number of gmres iterations in info.it.
%
%   See also MOR_SAMPLING, EVALTF.
%
%   Author:  Roel Van Beeumen
%   Version: April 11, 2017

%% dimensions
n = size(M,1);
k = length(shifts);

%% check inputs
if nargin < 5, V0 = []; end
if nargin < 6, use_gmres = 0; MK = M*K; end
if use_gmres
    restart = [];
    tol = [];
    maxit = min(n,500);
    P = diagprecond(M,K);
    x0 = [];
    if nargin == 7
        if isfield(opts,'restart'), restart = opts.restart; end
        if isfield(opts,'tol'), tol = opts.tol; end
        if isfield(opts,'maxit'), maxit = opts.maxit; end
        if isfield(opts,'P'), P = opts.P; end
        if isfield(opts,'x0'), x0 = opts.x0; end
    end
end

%% construct subspace
V1 = zeros(n,k);
if use_gmres
    it = zeros(k,1);
    parfor i = 1:k
        fprintf('i = %3i:',i);
        P1 = shifts(i)*speye(n) - P;
        funA = @(x) shifts(i)*x - M*(K*x);
        [w,flag,relres,iter] = gmres(funA,d/norm(d),restart,tol,maxit,P1,[],x0);
        fprintf('   flag = %i, res = %1.3e, iter = %i\n',flag,relres,iter(2));
        V1(:,i) = w/norm(w);
        it(i) = iter(2);
    end
else
    parfor i = 1:k
        if n > 500, fprintf('i = %3i\n',i); end
        w = (shifts(i)*speye(n) - MK)\(d/norm(d));
        V1(:,i) = w/norm(w);
    end
end
[V,~] = qr([V0,V1],0);

%% construct reduced order model
At = V'*(K*(M*(K*V)));
Et = V'*(K*V);
bt = V'*(K*d);
ct = bt;
if nargout > 5
    if use_gmres
        info = struct('it',sum(it));
    else
        info = [];
    end
end

end


function P = diagprecond(M,K)

n = size(M,1);
P = zeros(n,1);
for i = 1:n
    P(i) = M(i,:)*K(:,i);
end
P = spdiags(P,0,n,n);

end
