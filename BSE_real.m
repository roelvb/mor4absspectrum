function [Lambda,X1,X2] = BSE_real(A,B)
%BSE_REAL   Eigenvalues and eigenvectors
%   [Lambda,X1,X2] = BSE_REAL(A,B) returns the eigenvalues and corresponding
%   eigenvectors of the matrix
%                                                               T
%       [  A   B ] = [ X1  X2 ] [ Lambda          ] [  X1  -X2 ]
%       [ -B  -A ]   [ X2  X1 ] [         -Lambda ] [ -X2   X1 ]
%
%   with
%                   T
%       [  X1  -X2 ] [ X1  X2 ] = [ I  0 ]
%       [ -X2   X1 ] [ X2  X1 ]   [ 0  I ]
%
%   See also BSE_complex.

%   Reference:
%   M. Shao and C. Yang. BSEPACK User's Guide.
%   Technical Report, arXiv:1612.07848, 2016.

M = A + B;
K = A - B;
L1 = chol(M,'lower');
W = L1'*K*L1;
W = (W + W')/2;
[V,D] = eig(W);
Lambda = sqrt(D);
if nargout > 1
    Phi = L1*V;
    Psi = L1'\V;
    X1 = (Psi*Lambda + Phi)/2/sqrt(Lambda);
    X2 = (Psi*Lambda - Phi)/2/sqrt(Lambda);
end

end
