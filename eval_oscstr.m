function [eval,oscstr] = eval_oscstr(A,B,dx,dy,dz)
%EVAL_OSCSTR   Eigenvalues and oscillator strengths
%   [eval,oscstr] = EVAL_OSCSTR(A,B,dx,dy,dz,omega,eta,k) returns the
%   eigenvalues and oscillator strengths. A is Hermitian, B is complex
%   symmetric, and dx, dy, and dz are dipole vectors of the same size
%   as A and B.
%
%   See also BSE_real, BSE_complex.
%
%   Author:  Roel Van Beeumen
%   Version: September 1, 2017

% problem size
n = size(A,1);

% positive eigenvalues of H
if isreal(A) && isreal(B)
    [Lambda,X1,X2] = BSE_real(A,-B);
else
    [Lambda,X1,X2] = BSE_complex(A,-B);
end
eval = diag(Lambda);

% scale eigenvalues
Eh2eV = 27.21138602;
eval = eval*Eh2eV;

% dipole vectors
D = [dx,dy,dz];

% transition dipole
if isreal(A) && isreal(B)
    d = D'*(X1 - X2);
else
    d = D'*X1 - D.'*X2;
end

% oscillator strength
oscstr = zeros(n,1);
for i = 1:n
    oscstr(i) = norm(d(:,i))^2;
end

end
