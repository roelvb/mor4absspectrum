MOR algorithm for estimating the absorption spectrum
====================================================

MATLAB toolbox for estimating the absorption spectrum. The adaptive model order
reduction algorithm is general to any spectral domain, but its power is in those
spectral domains which are dense and interior in the eigenspectrum.


Examples
--------

* __`run_5H2O.m`__  
_Numerical experiment for the evaluation of the XAS spectrum of 5 H2O clusters
 by the MOR algorithm with adaptively chosen complex interpolation frequencies.
 The MOR results are compared to the Lorentzian broadened poles of the
 propagator, labeled eigensystem. A damping parameter of 1 eV was chosen both
 for the MOR calculations and the broadening factor of the Lorentzians for the
 reference._

* __`run_real_vs_complex_shifts_fixed.m`__  
_Numerical experiments for the evaluation of the XAS spectrum of 5 H2O clusters
 by the MOR algorithm using an adaptively chosen model order k. The MOR  results
 are compared to the Lorentzian broadened poles of the propagator, labeled
 eigensystem. A damping parameter of 1 eV was chosen both for the MOR
 calculations and the broadening factor of the Lorentzians for the reference.
 It can be seen that the use of complex interpolation frequencies for the
 construction of the model basis is important in spectrally dense regions._

* __`run_real_vs_complex_shifts_adaptive.m`__  
_Numerical experiments for the evaluation of the XAS spectrum of 5 H2O clusters
 by the MOR algorithm using a fixed model order (k = 32). The MOR results are
 compared to the Lorentzian broadened poles of the propagator, labeled
 eigensystem. A damping parameter of 1 eV was chosen both for the MOR
 calculations and the broadening factor of the Lorentzians for the reference.
 It can be seen that the use of complex interpolation frequencies for the
 construction of the model basis is important in spectrally dense regions._


Reference
---------

* [R. Van Beeumen](http://www.roelvanbeeumen.be), D.B. Williams-Young, J.M. Kasper, C. Yang, E.G. Ng, and X. Li,  
_Model order reduction algorithm for estimating the absorption spectrum_,  
[Journal of Chemical Theory and Computation](http://dx.doi.org/10.1021/acs.jctc.7b00402), 13(10), 4950-4961, 2017.
