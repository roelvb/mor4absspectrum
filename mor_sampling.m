function [At,Et,bt,ct,V,info] = mor_sampling(A,E,b,c,shifts,V0,use_gmres,opts)
%MOR_SAMPLING   Moment matching model order reduction via sampling
%   [At,Et,bt,ct] = MOR_SAMPLING(A,E,b,c,shifts) returns the reduced order
%   model which matches moments in the points given by shifts. The dimension
%   of the reduced order model is equal to the length of shifts.
%
%   MOR_SAMPLING(A,E,b,c,shifts,V0) expands the constructed subspace with V0.
%   The dimension of the reduced order model is in this case equal to the sum
%   of the length of shifts and the number of columns of V0.
%
%   MOR_SAMPLING(A,E,b,c,shifts,use_gmres) uses gmres to solve the linear
%   systems if use_gmres is true. Otherwise \ is used.
%
%   MOR_SAMPLING(A,E,b,c,shifts,use_gmres,opts) specifies the gmres
%   parameters:
%     opts.restart:  gmres restart parameter
%                    [ [] ]
%     opts.tol:      gmres tolerance parameter
%                    [ [] ]
%     opts.maxit:    gmres maximum number of outer iterations parameter
%                    [ 100 ]
%     opts.PA:       preconditioner for A
%                    [ spdiags(diag(A),0,n,n) ]
%     opts.PE:       preconditioner for E
%                    [ E ]
%     opts.x0:       gmres first initial guess parameter
%                    [ [] ]
%
%   [At,Et,bt,ct,V,info] = MOR_SAMPLING(A,E,b,c,shifts,...) also returns the
%   number of gmres iterations in info.it.
%
%   See also MOR_SAMPLING_MK, EVALTF.
%
%   Author:  Roel Van Beeumen
%   Version: April 11, 2017

%% dimensions
n = size(A,1);
k = length(shifts);

%% check inputs
if nargin < 6, V0 = []; end
if nargin < 7, use_gmres = 0; end
if use_gmres
    restart = [];
    tol = [];
    maxit = min(n,500);
    PA = spdiags(diag(A),0,n,n);
    PE = E;
    x0 = [];
    if nargin == 8
        if isfield(opts,'restart'), restart = opts.restart; end
        if isfield(opts,'tol'), tol = opts.tol; end
        if isfield(opts,'maxit'), maxit = opts.maxit; end
        if isfield(opts,'PA'), PA = opts.PA; end
        if isfield(opts,'PE'), PE = opts.PE; end
        if isfield(opts,'x0'), x0 = opts.x0; end
    end
end

%% construct subspace
V1 = zeros(n,k);
if use_gmres
    it = zeros(k,1);
    parfor i = 1:k
        fprintf('i = %3i:',i);
        M = shifts(i)*PE - PA;
        [w,flag,relres,iter] = gmres(shifts(i)*E - A,b/norm(b),restart,tol,maxit,M,[],x0);
        fprintf('   flag = %i, res = %1.3e, iter = %i\n',flag,relres,iter(2));
        V1(:,i) = w/norm(w);
        it(i) = iter(2);
    end
else
    parfor i = 1:k
        if n > 500, fprintf('i = %3i\n',i); end
        w = (shifts(i)*E - A)\(b/norm(b));
        V1(:,i) = w/norm(w);
    end
end
[V,~] = qr([V0,V1],0);

%% construct reduced order model
At = V'*A*V;
Et = V'*E*V;
bt = V'*b;
ct = V'*c;
if nargout > 5
    if use_gmres
        info = struct('it',sum(it));
    else
        info = [];
    end
end

end
