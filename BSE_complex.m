function [Lambda,X1,X2] = BSE_complex(A,B)
%BSE_COMPLEX   Eigenvalues and eigenvectors
%   [Lambda,X1,X2] = BSE_COMPLEX(A,B) returns the eigenvalues and corresponding
%   eigenvectors of the matrix
%
%       [    A         B     ] [ X1  conj(X2) ]
%       [ -conj(B)  -conj(A) ] [ X2  conj(X1) ]
%                                                                              H
%                     = [ X1  conj(X2) ] [ Lambda          ] [  X1  -conj(X2) ]
%                       [ X2  conj(X1) ] [         -Lambda ] [ -X2   conj(X1) ]
%
%   with
%
%                         H
%       [  X1  -conj(X2) ] [ X1  X2 ] = [ I  0 ]
%       [ -X2   conj(X1) ] [ X2  X1 ]   [ 0  I ]
%
%   See also BSE_real.

%   Reference:
%   M. Shao and C. Yang. BSEPACK User's Guide.
%   Technical Report, arXiv:1612.07848, 2016.

n = size(A,1);
I = eye(n);
Q = [I, -1i*I; I, 1i*I]/sqrt(2);
At = [real(A), imag(A); -imag(A), real(A)];
Bt = [real(B),-imag(B); -imag(B),-real(B)];
M = At + Bt;
L = chol(M,'lower');
J = [0*I,I;-I,0*I];
W = L'*J*L;
W = (W - W')/2;
[U,T] = hess(W);
t = -diag(T,-1);
T = diag(t,1) + diag(t,-1);
[V,D] = eig(T);
Lambda = D(n+1:end,n+1:end);
if nargout > 1
    X = Q*(L'\U)*diag(1i.^(0:2*n-1))*V;
    X1 = X(1:n,n+1:end)*sqrt(Lambda);
    X2 = X(n+1:end,n+1:end)*sqrt(Lambda);
end

end
